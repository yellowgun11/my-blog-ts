import { useEffect } from 'react'
import { connect } from 'react-redux'
import { RouteComponentProps } from 'react-router-dom';
import { Dispatch, bindActionCreators } from 'redux'
import { Tag } from "antd";
import {
  EyeOutlined
} from '@ant-design/icons';
import { DetailState, actions } from '../../redux/modules/detail'
import { CombinedState } from '../../redux/modules/index'
import './index.css'

function Detail(props: Props) {
  useEffect(() => {
    console.log(props)
    const { location, getDetailData } = props
    const id = location.pathname.split('/detail/')[1]
    getDetailData(id)
  }, [])
  return (
    <div className="detail">
      <div className='detail-banner'>
        <img src={props.detialData.imgUrl} alt="" />
      </div>
      <div className='detail-container'>
        <h1 className='detail-container__title'>{props.detialData.title}</h1>
        <div className='detail-container__top'>
          <div className='detail-container__left'>
            <div className='content_box_text_tag'>
              {
                props.detialData.tag && props.detialData.tag.map(value => {
                  return <Tag key={value} color="#87d068">{value}</Tag>
                })
              }
            </div>
            <div className='detail-container__read'>
              <EyeOutlined style={{ color:"lightblue" }} />
              <span className="detail-container__read_num">{props.detialData.readNum}</span>
            </div>
          </div>
          <div className='detail-container__time'>
            {props.detialData.createTime}
          </div>
        </div>
      </div>
      {
        props.detialData.content &&
        <div dangerouslySetInnerHTML={{ __html: props.detialData.content }} />
      }
    </div>
  )
}

const mapStateToProps = (state: CombinedState): DetailState => {
  return {
    ...state.detailReducer
  }
};
const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    ...bindActionCreators(actions, dispatch)
  }
}

type Props = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps> & RouteComponentProps

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Detail)